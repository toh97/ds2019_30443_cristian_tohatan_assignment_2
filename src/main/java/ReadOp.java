import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReadOp {

    private static String filePath = "src/main/resources/activity2.txt";
    private BufferedReader reader;
    private List<String> theLines = new ArrayList<>();

    public ReadOp() {
        FileReader fileReader;
        try{
            fileReader = new FileReader(filePath);
            this.reader = new BufferedReader(fileReader);
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
        this.theLines = readTheLines();
    }

    private List<String> readTheLines() {
        String line;
        try{
            while((line = this.reader.readLine()) != null){
                theLines.add(line);
            }
        }catch(IOException e){
            e.printStackTrace();
        }

        return theLines;
    }

    public List<String> getTheLines() {
        return theLines;
    }

    public void setTheLines(List<String> theLines) {
        this.theLines = theLines;
    }
}
