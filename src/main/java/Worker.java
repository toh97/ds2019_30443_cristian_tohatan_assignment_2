import java.util.ArrayList;
import java.util.List;

public class Worker {

    private List<Activity> activities = new ArrayList<>();

    public Worker(){
        ReadOp reader = new ReadOp();
        List<String> content = reader.getTheLines();
        ParseOp parser = new ParseOp();

        this.activities = parser.parseWorker(content);
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }
}
