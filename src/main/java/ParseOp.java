import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ParseOp {

    private String beautifyName(String name){

        return name.replace("\t","");
    }

    private Activity parseActivity(String givenActivity) {

        List<String> activityInfo = Stream.of(givenActivity.split("\t\t")).collect(Collectors.toList());
        /*
        List<String> startDate = Stream.of(activityInfo.get(0).split(" ")).collect(Collectors.toList());
        List<String> endDate = Stream.of(activityInfo.get(1).split(" ")).collect(Collectors.toList());
        String activityName = beautifyName(activityInfo.get(2));

        List<String> startDateSplit = Stream.of(startDate.get(0).split("-")).collect(Collectors.toList());
        List<String> startHourSplit = Stream.of(startDate.get(1).split(":")).collect(Collectors.toList());

        List<String> endDateSplit = Stream.of(endDate.get(0).split("-")).collect(Collectors.toList());
        List<String> endHourSplit = Stream.of(endDate.get(1).split(":")).collect(Collectors.toList());
*/

        String startTime = activityInfo.get(0);
        startTime.replace("T"," ");
        String endTime = activityInfo.get(1);
        endTime.replace("T"," ");
        String activityName = activityInfo.get(2);
        activityName = beautifyName(activityName);
        /*
        LocalDateTime startTime = LocalDateTime
                .of(Integer.parseInt(startDateSplit.get(0)), Integer.parseInt(startDateSplit.get(1)), Integer.parseInt(startDateSplit.get(2)),
                        Integer.parseInt(startHourSplit.get(0)),Integer.parseInt(startHourSplit.get(1)), Integer.parseInt(startHourSplit.get(2)));
        LocalDateTime endTime = LocalDateTime
                .of(Integer.parseInt(endDateSplit.get(0)), Integer.parseInt(endDateSplit.get(1)), Integer.parseInt(endDateSplit.get(2)),
                        Integer.parseInt(endHourSplit.get(0)),Integer.parseInt(endHourSplit.get(1)), Integer.parseInt(endHourSplit.get(2)));
*/
        return new Activity(startTime, endTime, activityName);
    }

    public ParseOp() {
    }

    public List<Activity> parseWorker(List<String> content){

        List<Activity> activities = new ArrayList<>();

        for(String activity: content){
            activities.add(parseActivity(activity));
        }

        return activities;
    }
}
