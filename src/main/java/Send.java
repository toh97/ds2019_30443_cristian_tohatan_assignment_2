import com.google.gson.Gson;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class Send {

    private final static String QUEUE_NAME = "ass2_test";
    private Worker slave;
    private static Gson gson = new Gson();

    public Send(){
        this.slave = new Worker();
    }

    public Worker getSlave() {
        return slave;
    }

    public void setSlave(Worker slave) {
        this.slave = slave;
    }

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            Send sender = new Send();
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);

            int i=1;
            for(Activity activity: sender.getSlave().getActivities()){

                //String message = gson.toJson(activity);
                String message = activity.toString();
                channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
                System.out.println("["+i+"]"+"Activity sent: "+message);
                Thread.sleep(100);
                ++i;
            }
        }
    }

}
