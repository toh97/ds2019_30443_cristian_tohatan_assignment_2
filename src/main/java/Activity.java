
public class Activity {

    private String startDate;
    private String endDate;
    private String activityName;

    public Activity(String startDate, String endDate, String activityName) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.activityName = activityName;
    }

    public String getStartDate() {
        return startDate;
    }

    public Activity() {
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    @Override
    public String toString() {
        return  "{\"startDate\": \"" + startDate +
                "\", \"endDate\": \"" + endDate +
                "\", \"activityName\": \"" + activityName +
                "\"}";
    }
}
