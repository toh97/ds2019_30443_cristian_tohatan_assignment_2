package com.example.springdemo.entities;
import javax.persistence.*;
import java.util.List;
import static javax.persistence.GenerationType.IDENTITY;

@Entity(name = "MedicationPlan")
@Table(name = "medication_plan")
public class MedicationPlan {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", length = 100, unique=true, nullable = false)
    private String name;

    @Column(name = "times_a_day")
    private int timesADay;

    @Column(name = "hour_interval")
    private int hourInterval;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "med_to_medplan",
            joinColumns = {@JoinColumn(name="medication_plan_id")},
            inverseJoinColumns = {@JoinColumn(name="medication_id")})
    private List<Medication> medicationList;

    @ManyToOne
    @JoinColumn
    private Patient patient;

    public MedicationPlan() {
    }

    public MedicationPlan(int id, String name, List<Medication> medicationList) {
        this.id = id;
        this.name = name;
        this.medicationList = medicationList;
    }

    public MedicationPlan(String name, int timesADay, int hourInterval, List<Medication> medicationList) {
        this.name = name;
        this.timesADay = timesADay;
        this.hourInterval = hourInterval;
        this.medicationList = medicationList;
    }

    public MedicationPlan(int id, String name, int timesADay, int hourInterval, List<Medication> medicationList) {
        this.id = id;
        this.name = name;
        this.timesADay = timesADay;
        this.hourInterval = hourInterval;
        this.medicationList = medicationList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Medication> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(List<Medication> medicationList) {
        this.medicationList = medicationList;
    }

    public int getTimesADay() {
        return timesADay;
    }

    public void setTimesADay(int timesADay) {
        this.timesADay = timesADay;
    }

    public int getHourInterval() {
        return hourInterval;
    }

    public void setHourInterval(int hourInterval) {
        this.hourInterval = hourInterval;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
