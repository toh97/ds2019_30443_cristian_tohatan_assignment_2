package com.example.springdemo.entities;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity(name = "Caregiver")
@Table(name = "caregiver")
public class Caregiver {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "email", nullable = false, unique=true, length = 200)
    private String email;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "caregiver", fetch = FetchType.LAZY)
    private List<Patient> patients;

    public Caregiver() {
    }

    public Caregiver(Integer id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public Caregiver(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}
