package com.example.springdemo.controller;

import com.example.springdemo.dto.patientDTOs.PatientDTO;
import com.example.springdemo.dto.patientDTOs.PatientViewDTO;
import com.example.springdemo.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping(value = "/{id}")
    public PatientViewDTO findById(@PathVariable("id") Integer id){
        return patientService.findPatientById(id);
    }

    @GetMapping()
    public List<PatientViewDTO> findAll(){
        return patientService.findAll();
    }

    @PostMapping()
    public Integer insertPatientDTO(@RequestBody PatientDTO patientDTO){
        return patientService.insert(patientDTO);
    }

    @PutMapping()
    public Integer updatePatient(@RequestBody PatientDTO patientDTO) {
        return patientService.update(patientDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody PatientViewDTO patientViewDTO){
        patientService.delete(patientViewDTO);
    }
}