package com.example.springdemo.controller;

import com.example.springdemo.dto.medPlanDTOs.MedicationPlanDTO;
import com.example.springdemo.dto.medPlanDTOs.MedicationPlanViewDTO;
import com.example.springdemo.services.MedicationPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicationPlan")
public class MedicationPlanController {
    private MedicationPlanService medicationPlanService;

    @Autowired
    public void setMedicationPlanService(MedicationPlanService medicationPlanService) {
        this.medicationPlanService = medicationPlanService;
    }

    //TODO: to be implemented


    @GetMapping(value = "/{id}")
    public MedicationPlanViewDTO findById(@PathVariable("id") Integer id){
        return medicationPlanService.findMedicationPlanById(id);
    }

    @GetMapping()
    public List<MedicationPlanDTO> findAll(){
        return medicationPlanService.findAll();
    }

    @PostMapping()
    public Integer insertMedicationPlanDTO(@RequestBody MedicationPlanDTO medicationPlanDTO){
        return medicationPlanService.insert(medicationPlanDTO);
    }

    @PutMapping()
    public Integer updateMedicationPlan(@RequestBody MedicationPlanDTO medicationPlanDTO) {
        return medicationPlanService.update(medicationPlanDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody MedicationPlanViewDTO medicationPlanViewDTO) {
        medicationPlanService.delete(medicationPlanViewDTO);
    }
}
