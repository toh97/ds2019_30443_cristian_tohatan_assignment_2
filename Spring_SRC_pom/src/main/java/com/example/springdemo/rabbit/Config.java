package com.example.springdemo.rabbit;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class Config {

    private final static String QUEUE_NAME = "ass2";

    @Bean
    public Queue queue(){
        return new Queue(QUEUE_NAME);
    }

    @Profile("receiver")
    @Bean
    public Receiver receiver(){
        return new Receiver();
    }

}