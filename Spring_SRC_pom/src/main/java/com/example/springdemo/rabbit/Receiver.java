package com.example.springdemo.rabbit;

import com.example.springdemo.entities.Activity;
import com.example.springdemo.services.ActivityService;
import com.google.gson.Gson;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class Receiver {

    @Autowired
    private ActivityService activityService;

    private static LocalDateTime stringToLdt(String date){
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(date, format);
    }

    public static Activity generateEntityFromString(String messageString) {
        Object obj = null;

        try {
            obj = new JSONParser().parse(messageString);
            JSONObject jo = (JSONObject) obj;

            LocalDateTime startTime = stringToLdt(jo.get("startDate").toString());
            LocalDateTime endTime = stringToLdt(jo.get("endDate").toString());
            return new Activity(
                    jo.get("activityName").toString(),
                    startTime,
                    endTime
            );
        } catch (org.json.simple.parser.ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RabbitListener(queues = "ass2_test")
    public void receive(byte[] activityReceived){
        String patientActivity = new String(activityReceived, StandardCharsets.UTF_8);
        //Activity activity = gson.fromJson(patientActivity, Activity.class);
        System.out.println(patientActivity );
        Activity activity = generateEntityFromString(patientActivity);
        activityService.insert(activity);
    }

}
