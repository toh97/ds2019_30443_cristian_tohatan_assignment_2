package com.example.springdemo.dto.builders.doctorBuilders;

import com.example.springdemo.dto.doctorDTOs.DoctorDTO;
import com.example.springdemo.entities.Doctor;

public class DoctorBuilder {

    private DoctorBuilder() {
    }

    public static DoctorDTO generateDTOFromEntity(Doctor doctor){
        return new DoctorDTO(
                doctor.getId(),
                doctor.getName(),
                doctor.getEmail(),
                doctor.getIban(),
                doctor.getAddress());
    }

    public static Doctor generateEntityFromDTO(DoctorDTO doctorDTO){
        return new Doctor(
                doctorDTO.getId(),
                doctorDTO.getName(),
                doctorDTO.getEmail(),
                doctorDTO.getIban(),
                doctorDTO.getAddress());
    }
}
