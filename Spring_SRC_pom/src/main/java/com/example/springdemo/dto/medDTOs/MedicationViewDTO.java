package com.example.springdemo.dto.medDTOs;



/* Although this class is, at this point, identical with MedicationDTO,
*  it is implemented here for further development, in the idea that,
*  in the future, some extra information, such as possible side-effects
*  will be displayed to the user when a medication is accessed
* */

public class MedicationViewDTO {

    private Integer id;
    private String name;
    private Integer price;

    public MedicationViewDTO(Integer id, String name, Integer price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

}
