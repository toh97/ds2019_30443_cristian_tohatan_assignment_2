package com.example.springdemo.dto.patientDTOs;

public class PatientViewDTO {
    private Integer id;
    private String name;
    private String sex;
    private Integer age;
    private String cnp;
    private String address;

    public PatientViewDTO(Integer id, String name, String sex, Integer age, String cnp, String address) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.cnp = cnp;
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
