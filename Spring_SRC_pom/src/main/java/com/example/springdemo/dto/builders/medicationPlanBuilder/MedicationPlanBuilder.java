package com.example.springdemo.dto.builders.medicationPlanBuilder;

import com.example.springdemo.dto.medPlanDTOs.MedicationPlanDTO;
import com.example.springdemo.entities.MedicationPlan;

public class MedicationPlanBuilder {
    private MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO generateDTOFromEntity(MedicationPlan medicationPlan) {
        return new MedicationPlanDTO(
                medicationPlan.getId(),
                medicationPlan.getName(),
                medicationPlan.getTimesADay(),
                medicationPlan.getHourInterval(),
                medicationPlan.getMedicationList());
    }

    public static MedicationPlan generateEntityFromDTO(MedicationPlanDTO medicationPlanDTO) {
        return new MedicationPlan(
              medicationPlanDTO.getId(),
                medicationPlanDTO.getName(),
                medicationPlanDTO.getTimesADay(),
                medicationPlanDTO.getHourInterval(),
                medicationPlanDTO.getMedicationList()
        );
    }
}
