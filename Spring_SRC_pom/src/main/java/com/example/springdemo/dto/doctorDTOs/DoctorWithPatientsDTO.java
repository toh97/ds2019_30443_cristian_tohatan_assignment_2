package com.example.springdemo.dto.doctorDTOs;

import com.example.springdemo.dto.patientDTOs.PatientDTO;

import java.util.List;

public class DoctorWithPatientsDTO {
    private Integer id;
    private String name;
    private List<PatientDTO> patients;

    public DoctorWithPatientsDTO(){}

    public DoctorWithPatientsDTO(Integer id, String name, List<PatientDTO> patients) {
        this.id = id;
        this.name = name;
        this.patients = patients;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PatientDTO> getPatients() {
        return patients;
    }

    public void setPatients(List<PatientDTO> patients) {
        this.patients = patients;
    }
}
