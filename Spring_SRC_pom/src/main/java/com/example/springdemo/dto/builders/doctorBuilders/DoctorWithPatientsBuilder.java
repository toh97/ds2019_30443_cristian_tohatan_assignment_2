package com.example.springdemo.dto.builders.doctorBuilders;

import com.example.springdemo.dto.builders.patientBuilder.PatientBuilder;
import com.example.springdemo.dto.patientDTOs.PatientDTO;
import com.example.springdemo.dto.doctorDTOs.DoctorWithPatientsDTO;
import com.example.springdemo.entities.Doctor;
import com.example.springdemo.entities.Patient;

import java.util.List;
import java.util.stream.Collectors;

public class DoctorWithPatientsBuilder {
    private DoctorWithPatientsBuilder(){}

    public static DoctorWithPatientsDTO generateDTOFromEntity(Doctor doctor, List<Patient> patients){
        List<PatientDTO> dtos =  patients.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());

        return new DoctorWithPatientsDTO(
               doctor.getId(), doctor.getName(),dtos);
    }

}
