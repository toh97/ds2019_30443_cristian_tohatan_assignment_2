package com.example.springdemo.dto.builders.medicationPlanBuilder;

import com.example.springdemo.dto.medPlanDTOs.MedicationPlanViewDTO;
import com.example.springdemo.entities.MedicationPlan;

public class MedicationPlanViewBuilder {

    private MedicationPlanViewBuilder(){}

    public static MedicationPlanViewDTO generateDTOFromEntity(MedicationPlan medicationPlan){
        return new MedicationPlanViewDTO(
                medicationPlan.getId(),
                medicationPlan.getName(),
                medicationPlan.getMedicationList());
    }

    public static MedicationPlan generateEntityFromDTO(MedicationPlanViewDTO medicationPlanViewDTO){
        return new MedicationPlan(
                medicationPlanViewDTO.getId(),
                medicationPlanViewDTO.getName(),
                medicationPlanViewDTO.getMedicationList());
    }
}
