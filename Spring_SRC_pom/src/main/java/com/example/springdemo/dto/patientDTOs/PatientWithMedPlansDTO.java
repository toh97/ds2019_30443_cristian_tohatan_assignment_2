package com.example.springdemo.dto.patientDTOs;

import com.example.springdemo.dto.medPlanDTOs.MedicationPlanDTO;
import com.example.springdemo.entities.MedicationPlan;

import java.util.List;

public class PatientWithMedPlansDTO {

    private Integer id;
    private String name;
    private List<MedicationPlanDTO> medicationPlanList;

    public PatientWithMedPlansDTO(){}

    public PatientWithMedPlansDTO(Integer id, String name, List<MedicationPlanDTO> medicationPlanList) {
        this.id = id;
        this.name = name;
        this.medicationPlanList = medicationPlanList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MedicationPlanDTO> getMedicationPlanList() {
        return medicationPlanList;
    }

    public void setMedicationPlanList(List<MedicationPlanDTO> medicationPlanList) {
        this.medicationPlanList = medicationPlanList;
    }
}
