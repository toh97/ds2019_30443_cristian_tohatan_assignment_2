package com.example.springdemo.dto.medPlanDTOs;

import com.example.springdemo.entities.Medication;
import com.example.springdemo.entities.Patient;

import java.util.List;

public class MedicationPlanDTO {
    private Integer id;
    private String name;
    private Integer timesADay;
    private Integer hourInterval;
    private List<Medication> medicationList;
    private Integer patientId;

    public MedicationPlanDTO() {
    }

    public MedicationPlanDTO(Integer id, String name, Integer timesADay, Integer hourInterval, List<Medication> medicationList) {
        this.id = id;
        this.name = name;
        this.timesADay = timesADay;
        this.hourInterval = hourInterval;
        this.medicationList = medicationList;
    }

    public List<Medication> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(List<Medication> medicationList) {
        this.medicationList = medicationList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTimesADay() {
        return timesADay;
    }

    public void setTimesADay(Integer timesADay) {
        this.timesADay = timesADay;
    }

    public Integer getHourInterval() {
        return hourInterval;
    }

    public void setHourInterval(Integer hourInterval) {
        this.hourInterval = hourInterval;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer id) {
        this.patientId = id;
    }
}
