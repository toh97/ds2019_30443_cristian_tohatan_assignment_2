package com.example.springdemo.dto.doctorDTOs;

import java.util.Objects;

public class DoctorDTO {

    private Integer id;
    private String name;
    private String email;
    private String iban;
    private String address;

    public DoctorDTO() {
    }

    public DoctorDTO(Integer id, String name, String email, String iban, String address) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.iban = iban;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoctorDTO doctorDTO = (DoctorDTO) o;
        return Objects.equals(id, doctorDTO.id) &&
                Objects.equals(name, doctorDTO.name) &&
                Objects.equals(email, doctorDTO.email) &&
                Objects.equals(iban, doctorDTO.iban) &&
                Objects.equals(address, doctorDTO.address);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, email, iban, address);
    }
}
