package com.example.springdemo.dto.builders.patientBuilder;

import com.example.springdemo.dto.patientDTOs.PatientViewDTO;
import com.example.springdemo.entities.Patient;

public class PatientViewBuilder {

    private PatientViewBuilder() {
    }

    public static PatientViewDTO generateDTOFromEntity(Patient patient){
        return new PatientViewDTO(
                patient.getId(),
                patient.getName(),
                patient.getSex(),
                patient.getAge(),
                patient.getCnp(),
                patient.getAddress());
    }

    public static Patient generateEntityFromDTO(PatientViewDTO patientDTO){
        return new Patient(
                patientDTO.getId(),
                patientDTO.getName(),
                patientDTO.getCnp(),
                patientDTO.getAge(),
                patientDTO.getSex(),
                patientDTO.getAddress());
    }


}
