package com.example.springdemo.dto.patientDTOs;

import com.example.springdemo.entities.MedicationPlan;

import java.util.List;
import java.util.Objects;

public class PatientDTO {

    private Integer id;
    private String  name;
    private String  cnp;
    private Integer age;
    private String  sex;
    private String address;
    private List<MedicationPlan> medicationPlanList;

    public PatientDTO(){}

    public PatientDTO(Integer id, String name, String cnp, Integer age, String sex, String address) {
        this.id = id;
        this.name = name;
        this.cnp = cnp;
        this.age = age;
        this.sex = sex;
        this.address = address;
    }

    public PatientDTO(String name, String cnp, Integer age, String sex, String address, List<MedicationPlan> medicationPlanList) {
        this.name = name;
        this.cnp = cnp;
        this.age = age;
        this.sex = sex;
        this.address = address;
        this.medicationPlanList = medicationPlanList;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public List<MedicationPlan> getMedicationPlanList() {
        return medicationPlanList;
    }

    public void setMedicationPlanList(List<MedicationPlan> medicationPlanList) {
        this.medicationPlanList = medicationPlanList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientDTO patientDTO = (PatientDTO) o;
        return Objects.equals(id, patientDTO.id) &&
                Objects.equals(name, patientDTO.name) &&
                Objects.equals(cnp, patientDTO.cnp) &&
                Objects.equals(sex, patientDTO.sex) &&
                Objects.equals(age, patientDTO.age) &&
                Objects.equals(address, patientDTO.address);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
