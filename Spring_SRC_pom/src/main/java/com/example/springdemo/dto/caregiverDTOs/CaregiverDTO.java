package com.example.springdemo.dto.caregiverDTOs;

import java.util.Objects;

public class CaregiverDTO {

    private Integer id;
    private String name;
    private String email;

    public CaregiverDTO() {
    }

    public CaregiverDTO(Integer id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaregiverDTO caregiverDTO = (CaregiverDTO) o;
        return Objects.equals(id, caregiverDTO.id) &&
                Objects.equals(name, caregiverDTO.name) &&
                Objects.equals(email, caregiverDTO.email);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, email);
    }
}