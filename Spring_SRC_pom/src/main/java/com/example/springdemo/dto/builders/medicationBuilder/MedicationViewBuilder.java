package com.example.springdemo.dto.builders.medicationBuilder;

import com.example.springdemo.dto.medDTOs.MedicationDTO;
import com.example.springdemo.dto.medDTOs.MedicationViewDTO;
import com.example.springdemo.entities.Medication;

public class MedicationViewBuilder {

    private MedicationViewBuilder() {
    }

    public static MedicationViewDTO generateDTOFromEntity(Medication medication) {
        return new MedicationViewDTO(
                medication.getId(),
                medication.getName(),
                medication.getPrice());
    }

    public static Medication generateEntityFromDTO(MedicationViewDTO medicationViewDTO) {
        return new Medication(
                medicationViewDTO.getId(),
                medicationViewDTO.getName(),
                medicationViewDTO.getPrice());
    }

}
