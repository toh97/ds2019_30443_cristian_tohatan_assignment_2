package com.example.springdemo.dto.builders.caregiverBuilders;

import com.example.springdemo.dto.caregiverDTOs.CaregiverViewDTO;
import com.example.springdemo.entities.Caregiver;

public class CaregiverViewBuilder {
    private CaregiverViewBuilder(){}

    public static CaregiverViewDTO generateDTOFromEntity(Caregiver caregiver){
        return new CaregiverViewDTO(
                caregiver.getId(),
                caregiver.getName(),
                caregiver.getEmail());
    }

    public static Caregiver generateEntityFromDTO(CaregiverViewDTO caregiverViewDTO){
        return new Caregiver(
                caregiverViewDTO.getId(),
                caregiverViewDTO.getName(),
                caregiverViewDTO.getEmail());
    }
}
