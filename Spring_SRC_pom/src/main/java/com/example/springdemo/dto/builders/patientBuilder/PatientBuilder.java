package com.example.springdemo.dto.builders.patientBuilder;

import com.example.springdemo.dto.patientDTOs.PatientDTO;
import com.example.springdemo.entities.Patient;

public class PatientBuilder {

    private PatientBuilder() {
    }

    public static PatientDTO generateDTOFromEntity(Patient patient){
        return new PatientDTO(
                patient.getId(),
                patient.getName(),
                patient.getCnp(),
                patient.getAge(),
                patient.getSex(),
                patient.getAddress());
    }

    public static Patient generateEntityFromDTO(PatientDTO patientDTO){
        return new Patient(
                patientDTO.getId(),
                patientDTO.getName(),
                patientDTO.getCnp(),
                patientDTO.getAge(),
                patientDTO.getSex(),
                patientDTO.getAddress());
    }
}
