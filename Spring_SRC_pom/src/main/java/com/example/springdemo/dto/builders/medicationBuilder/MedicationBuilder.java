package com.example.springdemo.dto.builders.medicationBuilder;

import com.example.springdemo.dto.medDTOs.MedicationDTO;
import com.example.springdemo.entities.Medication;

public class MedicationBuilder {

    private MedicationBuilder() {
    }

    public static MedicationDTO generateDTOFromEntity(Medication medication) {
        return new MedicationDTO(
                medication.getId(),
                medication.getName(),
                medication.getPrice());
    }

    public static Medication generateEntityFromDTO(MedicationDTO medicationDTO) {
        return new Medication(
                medicationDTO.getId(),
                medicationDTO.getName(),
                medicationDTO.getPrice());
    }
    
}
