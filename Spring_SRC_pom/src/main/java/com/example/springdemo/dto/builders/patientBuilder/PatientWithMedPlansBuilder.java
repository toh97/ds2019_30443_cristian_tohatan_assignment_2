package com.example.springdemo.dto.builders.patientBuilder;

import com.example.springdemo.dto.builders.medicationPlanBuilder.MedicationPlanBuilder;
import com.example.springdemo.dto.medPlanDTOs.MedicationPlanDTO;
import com.example.springdemo.dto.patientDTOs.PatientWithMedPlansDTO;
import com.example.springdemo.entities.MedicationPlan;
import com.example.springdemo.entities.Patient;

import java.util.List;
import java.util.stream.Collectors;

public class PatientWithMedPlansBuilder {

    private PatientWithMedPlansBuilder(){}

    public static PatientWithMedPlansDTO generateDTOFromEntity(Patient patient, List<MedicationPlan> medicationPlans){
        List<MedicationPlanDTO> dtos =  medicationPlans.stream()
                .map(MedicationPlanBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());

        return new PatientWithMedPlansDTO(
                patient.getId(), patient.getName(),dtos);
    }
    
}
