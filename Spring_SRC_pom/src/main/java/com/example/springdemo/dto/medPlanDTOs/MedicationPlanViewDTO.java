package com.example.springdemo.dto.medPlanDTOs;

import com.example.springdemo.entities.Medication;

import java.util.List;

public class MedicationPlanViewDTO {

    private Integer id;
    private String name;
    private List<Medication> medicationList;

    public MedicationPlanViewDTO(Integer id, String name, List<Medication> medicationList) {
        this.id = id;
        this.name = name;
        this.medicationList = medicationList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Medication> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(List<Medication> medicationList) {
        this.medicationList = medicationList;
    }
}
