package com.example.springdemo.validators;

import com.example.springdemo.dto.patientDTOs.PatientDTO;
import com.example.springdemo.errorhandler.IncorrectParameterException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class PatientFieldValidator {

    private static final Log LOGGER = LogFactory.getLog(PatientFieldValidator.class);
    private static final CnpFieldValidator CNP_VALIDATOR = new CnpFieldValidator() ;

    private PatientFieldValidator(){}

    public static void validateInsertOrUpdate(PatientDTO patientDTO) {

        List<String> errors = new ArrayList<>();
        if (patientDTO == null) {
            errors.add("patientDTO is null");
            throw new IncorrectParameterException(PatientDTO.class.getSimpleName(), errors);
        }
        if (patientDTO.getCnp() == null || !CNP_VALIDATOR.validate(patientDTO.getCnp())) {
            errors.add("Patient cnp has invalid format");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(PatientFieldValidator.class.getSimpleName(), errors);
        }
    }
    
    
}
