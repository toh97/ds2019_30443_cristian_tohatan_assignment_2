package com.example.springdemo.validators;

public class CnpFieldValidator implements FieldValidator<String>{

    @Override
    public boolean validate(String cnp) {

        System.out.println("CNP: "+cnp);
        if( !(cnp.charAt(0) == '1' || cnp.charAt(0) == '2' )) return false;

        return true;
    }
}
