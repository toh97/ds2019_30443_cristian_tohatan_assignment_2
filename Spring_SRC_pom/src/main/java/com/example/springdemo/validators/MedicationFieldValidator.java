package com.example.springdemo.validators;

import com.example.springdemo.dto.medDTOs.MedicationDTO;
import com.example.springdemo.errorhandler.IncorrectParameterException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class MedicationFieldValidator {

    private static final Log LOGGER = LogFactory.getLog(MedicationFieldValidator.class);

    private MedicationFieldValidator(){}

    public static void validateInsertOrUpdate(MedicationDTO medicationDTO) {
        List<String> errors = new ArrayList<>();
        if (medicationDTO == null) {
            errors.add("medicationDTO is null");
            throw new IncorrectParameterException(MedicationDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(MedicationFieldValidator.class.getSimpleName(), errors);
        }
    }
}
