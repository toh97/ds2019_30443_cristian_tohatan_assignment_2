package com.example.springdemo.validators;

import com.example.springdemo.dto.medPlanDTOs.MedicationPlanDTO;
import com.example.springdemo.errorhandler.IncorrectParameterException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class MedicationPlanFieldValidator {

    private static final Log LOGGER = LogFactory.getLog(MedicationPlanFieldValidator.class);

    private MedicationPlanFieldValidator(){}

    public static void validateInsertOrUpdate(MedicationPlanDTO medicationPlanDTO) {
        List<String> errors = new ArrayList<>();
        if (medicationPlanDTO == null) {
            errors.add("medicationPlanDTO is null");
            throw new IncorrectParameterException(MedicationPlanDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(MedicationPlanFieldValidator.class.getSimpleName(), errors);
        }
    }
}
