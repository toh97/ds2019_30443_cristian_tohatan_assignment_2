package com.example.springdemo.services;

import com.example.springdemo.dto.caregiverDTOs.CaregiverDTO;
import com.example.springdemo.dto.caregiverDTOs.CaregiverViewDTO;
import com.example.springdemo.dto.builders.caregiverBuilders.CaregiverBuilder;
import com.example.springdemo.dto.builders.caregiverBuilders.CaregiverViewBuilder;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.errorhandler.DuplicateEntryException;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.CaregiverRepository;
import com.example.springdemo.validators.CaregiverFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    public CaregiverViewDTO findCaregiverById(Integer id){
        Optional<Caregiver> caregiver  = caregiverRepository.findById(id);

        if (!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "user id", id);
        }
        return CaregiverViewBuilder.generateDTOFromEntity(caregiver.get());
    }

    public List<CaregiverViewDTO> findAll(){
        List<Caregiver> caregivers = caregiverRepository.findAll();

        return caregivers.stream()
                .map(CaregiverViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    /*
    public List<CaregiverWithPatientsDTO> findAllFetch(){
        List<Caregiver> caregiverList = caregiverRepository.getAllFetch();

        return caregiverList.stream()
                .map(x-> CaregiverWithPatientsBuilder.generateDTOFromEntity(x, x.getPatients()))
                .collect(Collectors.toList());
    }

    //WRONG - without fetch an additional query is executed for each FK
    public List<CaregiverWithPatientsDTO> findAllFetchWrong(){
        List<Caregiver> caregiverList = caregiverRepository.findAll();

        return caregiverList.stream()
                .map(x-> CaregiverWithPatientsBuilder.generateDTOFromEntity(x, x.getPatients()))
                .collect(Collectors.toList());
    }
*/

    public Integer insert(CaregiverDTO caregiverDTO) {
        CaregiverFieldValidator.validateInsertOrUpdate(caregiverDTO);

        Caregiver caregiver = caregiverRepository.findByEmail(caregiverDTO.getEmail());
        if(caregiver != null){
            throw  new DuplicateEntryException("Caregiver", "email", caregiverDTO.getEmail());
        }

        return caregiverRepository
                .save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO))
                .getId();
    }

    public Integer update(CaregiverDTO caregiverDTO) {

        Caregiver caregiver = caregiverRepository.findByEmail(caregiverDTO.getEmail());

        if(caregiver == null){
            throw new ResourceNotFoundException("Caregiver", "caregiver id", caregiverDTO.getEmail());
        }
        CaregiverFieldValidator.validateInsertOrUpdate(caregiverDTO);
        caregiverDTO.setId(caregiver.getId());

        return caregiverRepository.save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO)).getId();
    }

    public void delete(CaregiverViewDTO caregiverViewDTO){

        Caregiver caregiver = caregiverRepository.findByEmail(caregiverViewDTO.getEmail());

        if(caregiver == null){
            throw new ResourceNotFoundException("Caregiver", "caregiver id", caregiver.getEmail());
        }

        this.caregiverRepository.deleteById(caregiver.getId());
    }



}
