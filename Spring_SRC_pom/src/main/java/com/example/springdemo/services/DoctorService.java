package com.example.springdemo.services;

import com.example.springdemo.dto.doctorDTOs.DoctorDTO;
import com.example.springdemo.dto.doctorDTOs.DoctorViewDTO;
import com.example.springdemo.dto.doctorDTOs.DoctorWithPatientsDTO;
import com.example.springdemo.dto.builders.doctorBuilders.DoctorBuilder;
import com.example.springdemo.dto.builders.doctorBuilders.DoctorViewBuilder;
import com.example.springdemo.dto.builders.doctorBuilders.DoctorWithPatientsBuilder;
import com.example.springdemo.entities.Doctor;
import com.example.springdemo.errorhandler.DuplicateEntryException;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.DoctorRepository;
import com.example.springdemo.validators.DoctorFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DoctorService {

    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public DoctorViewDTO findDoctorById(Integer id){
        Optional<Doctor> doctor  = doctorRepository.findById(id);

        if (!doctor.isPresent()) {
            throw new ResourceNotFoundException("Doctor", "user id", id);
        }
        return DoctorViewBuilder.generateDTOFromEntity(doctor.get());
    }

    public List<DoctorViewDTO> findAll(){
        List<Doctor> doctors = doctorRepository.getAllOrdered();

        return doctors.stream()
                .map(DoctorViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public List<DoctorWithPatientsDTO> findAllFetch(){
        List<Doctor> doctorList = doctorRepository.getAllFetch();

        return doctorList.stream()
                .map(x-> DoctorWithPatientsBuilder.generateDTOFromEntity(x, x.getPatients()))
                .collect(Collectors.toList());
    }

    //WRONG - without fetch an additional query is executed for each FK
    public List<DoctorWithPatientsDTO> findAllFetchWrong(){
        List<Doctor> doctorList = doctorRepository.findAll();

        return doctorList.stream()
                .map(x-> DoctorWithPatientsBuilder.generateDTOFromEntity(x, x.getPatients()))
                .collect(Collectors.toList());
    }

    public Integer insert(DoctorDTO doctorDTO) {
        DoctorFieldValidator.validateInsertOrUpdate(doctorDTO);

        Doctor doctor = doctorRepository.findByEmail(doctorDTO.getEmail());
        if(doctor != null){
            throw  new DuplicateEntryException("Doctor", "email", doctorDTO.getEmail());
        }

        return doctorRepository
                .save(DoctorBuilder.generateEntityFromDTO(doctorDTO))
                .getId();
    }

    public Integer update(DoctorDTO doctorDTO) {

        Doctor doctor = doctorRepository.findByEmail(doctorDTO.getEmail());

        if(doctor == null) {
            throw new ResourceNotFoundException("Doctor", "resource not found", doctorDTO.getEmail());
        }
        DoctorFieldValidator.validateInsertOrUpdate(doctorDTO);

        doctorDTO.setId(doctor.getId());
        return doctorRepository.save(DoctorBuilder.generateEntityFromDTO(doctorDTO)).getId();
    }

    public void delete(DoctorViewDTO doctorViewDTO){

        Doctor doctor = doctorRepository.findByEmail(doctorViewDTO.getEmail());

        if(doctor == null){
            throw new ResourceNotFoundException("Doctor", "doctor email", doctorViewDTO.getEmail());
        }

        this.doctorRepository.deleteById(doctor.getId());
    }

}
