package com.example.springdemo.services;

import com.example.springdemo.dto.patientDTOs.PatientDTO;
import com.example.springdemo.dto.patientDTOs.PatientViewDTO;
import com.example.springdemo.dto.builders.patientBuilder.PatientBuilder;
import com.example.springdemo.dto.builders.patientBuilder.PatientViewBuilder;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.errorhandler.DuplicateEntryException;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.PatientRepository;
import com.example.springdemo.validators.PatientFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public PatientViewDTO findPatientById(Integer id) {
        Optional<Patient> patient = patientRepository.findById(id);

        if (!patient.isPresent()) {
            throw new ResourceNotFoundException("Patient", "user id", id);
        }
        return PatientViewBuilder.generateDTOFromEntity(patient.get());
    }

    public List<PatientViewDTO> findAll() {
        List<Patient> patients = patientRepository.findAll();

        return patients.stream()
                .map(PatientViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(PatientDTO patientDTO) {
        PatientFieldValidator.validateInsertOrUpdate(patientDTO);

        return patientRepository
                .save(PatientBuilder.generateEntityFromDTO(patientDTO))
                .getId();
    }

    public Integer update(PatientDTO patientDTO) {

        Patient patient = patientRepository.findByCnp(patientDTO.getCnp());
        if (patient == null) {
            throw new ResourceNotFoundException("Patient", "user cnp", patientDTO.getCnp().toString());
        }
        PatientFieldValidator.validateInsertOrUpdate(patientDTO);

        Patient patientToSave = new Patient(patientDTO.getName(), patientDTO.getCnp(),
                patientDTO.getSex(), patientDTO.getAge(), patientDTO.getAddress());
        patientToSave.setId(patient.getId());

        patientRepository.save(patientToSave);
        return 1;
    }

    public void delete(PatientViewDTO patientViewDTO) {

        Patient patient = patientRepository.findByCnp(patientViewDTO.getCnp());

        if (patient == null) {
            throw new ResourceNotFoundException("Patient", "user id", patientViewDTO.getCnp().toString());
        }

        patientViewDTO.setId(patient.getId());
        this.patientRepository.deleteById(patientViewDTO.getId());
    }
}