package com.example.springdemo.services;


import com.example.springdemo.dto.builders.medicationBuilder.MedicationBuilder;
import com.example.springdemo.dto.builders.medicationBuilder.MedicationViewBuilder;
import com.example.springdemo.dto.medDTOs.MedicationDTO;
import com.example.springdemo.dto.medDTOs.MedicationViewDTO;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.errorhandler.DuplicateEntryException;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.MedicationRepository;
import com.example.springdemo.validators.MedicationFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationService {
    private MedicationRepository medicationRepository;

    @Autowired
    public void setMedicationRepository(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    //TODO: to be implemented

    //Find by ID
    public MedicationViewDTO findMedicationById(Integer id){
        Optional<Medication> medication  = medicationRepository.findById(id);

        if (!medication.isPresent()) {
            throw new ResourceNotFoundException("Medication", "medication id", id);
        }
        return MedicationViewBuilder.generateDTOFromEntity(medication.get());
    }

    //Find All
    public List<MedicationViewDTO> findAll(){
        List<Medication> medications = medicationRepository.findAll();
        return medications.stream()
                .map(MedicationViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    //Insert
    public Integer insert(MedicationDTO medicationDTO){
        MedicationFieldValidator.validateInsertOrUpdate(medicationDTO);

        Medication medication = medicationRepository
                .findByName(medicationDTO.getName());
        if(medication != null){
            throw new DuplicateEntryException("Medication", "id",
                    medicationDTO.getId());
        }
        return medicationRepository
                .save(MedicationBuilder
                        .generateEntityFromDTO(medicationDTO))
                .getId();
    }

    //Update
    public Integer update(MedicationDTO medicationDTO){
        /*Optional<Medication> medicationPlan = medicationRepository.findById(medicationDTO.getId());
        if(!medicationPlan.isPresent()){
            throw new ResourceNotFoundException("Medication","id", medicationDTO.getId());
        }
        MedicationFieldValidator.validateInsertOrUpdate(medicationDTO);
        return medicationRepository.save(MedicationBuilder.generateEntityFromDTO(medicationDTO)).getId();*/

        Medication medication = medicationRepository.findByName(medicationDTO.getName());
        if(medication == null){
            throw new ResourceNotFoundException("not found!","name", medicationDTO.getName());
        }
        MedicationFieldValidator.validateInsertOrUpdate(medicationDTO);
        medicationDTO.setId(medication.getId());

        return medicationRepository.save(MedicationBuilder.generateEntityFromDTO(medicationDTO)).getId();
    }

    //Delete
    public void delete(MedicationViewDTO medicationViewDTO){
        Optional<Medication> medication = medicationRepository.findById(medicationViewDTO.getId());
        if(!medication.isPresent()){
            throw new ResourceNotFoundException("Medication", "id", medicationViewDTO.getId());
        }
        this.medicationRepository.deleteById(medicationViewDTO.getId());
    }
}
