package com.example.springdemo.services;

import com.example.springdemo.entities.Activity;
import com.example.springdemo.repositories.ActivityRepository;
import com.rabbitmq.tools.json.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.temporal.ChronoUnit;

@Service
public class ActivityService {

    private ActivityRepository activityRepository;

    @Autowired
    public ActivityService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public Integer insert(Activity activity) {

        System.out.println("INSERT");

        if(activity.getName().equals("Toileting")){
            verifyToileting(activity);
        }
        if(activity.getName().equals("Sleeping")){
            verifySleep(activity);
        }
        if(activity.getName().equals("Leaving")){
            verifyLeaving(activity);
        }

        return activityRepository
                .save(activity)
                .getId();
    }

    //R1
    public Integer verifySleep(Activity sleep){
        if(!sleep.getName().equals("Sleeping")){
            System.out.println("verifySleep violation: wrong activity got here!");
            return -1;
        }
        long hoursPassed = ChronoUnit.HOURS.between(sleep.getStartDate(), sleep.getEndDate());
        if(hoursPassed > 12){
            System.out.println("R1 VIOLATION: Sleep time > 12 hours");
            return -1;
        }
        System.out.println("Activity verified - sleep: R1 PASSED!");
        return 1;
    }

    //R2
    public Integer verifyLeaving(Activity leave){
        if(!leave.getName().equals("Leaving")){
            System.out.println("verifyLeaving violation: wrong activity got here!");
            return -1;
        }
        long hoursPassed = ChronoUnit.HOURS.between(leave.getStartDate(), leave.getEndDate());
        if(hoursPassed > 12){
            System.out.println("R1 VIOLATION: Outdoor time > 12 hours");
            return -1;
        }
        System.out.println("Activity verified - leaving: R2 PASSED!");
        return 1;
    }

    //R3
    public Integer verifyToileting(Activity groom){
        if(!groom.getName().equals("Toileting")){
            System.out.println("verifySleep violation: wrong activity got here!");
            return -1;
        }
        long hoursPassed = ChronoUnit.HOURS.between(groom.getStartDate(), groom.getEndDate());
        if(hoursPassed > 1){
            System.out.println("R1 VIOLATION: Toileting time > 1 hour");
            return -1;
        }
        System.out.println("Activity verified - toileting: R3 PASSED!");
        return 1;
    }
    
}
