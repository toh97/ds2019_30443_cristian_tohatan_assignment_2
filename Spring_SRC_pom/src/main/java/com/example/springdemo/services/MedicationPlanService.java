package com.example.springdemo.services;

import com.example.springdemo.dto.builders.medicationPlanBuilder.MedicationPlanBuilder;
import com.example.springdemo.dto.builders.medicationPlanBuilder.MedicationPlanViewBuilder;
import com.example.springdemo.dto.medPlanDTOs.MedicationPlanDTO;
import com.example.springdemo.dto.medPlanDTOs.MedicationPlanViewDTO;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.entities.MedicationPlan;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.errorhandler.DuplicateEntryException;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.MedicationPlanRepository;
import com.example.springdemo.repositories.PatientRepository;
import com.example.springdemo.validators.MedicationPlanFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {
    private MedicationPlanRepository medicationPlanRepository;
    private PatientRepository patientRepository;

    @Autowired
    public void setMedicationPlanRepository(MedicationPlanRepository medicationPlanRepository, PatientRepository patientRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
        this.patientRepository = patientRepository;
    }

    //TODO: to be implemented

    //Find By ID
    public MedicationPlanViewDTO findMedicationPlanById(Integer id){
        Optional<MedicationPlan> medicationPlan  = medicationPlanRepository.findById(id);

        if (!medicationPlan.isPresent()) {
            throw new ResourceNotFoundException("Medication Plan", "medication plan id", id);
        }
        return MedicationPlanViewBuilder.generateDTOFromEntity(medicationPlan.get());
    }

    //Find All
    public List<MedicationPlanDTO> findAll(){
        List<MedicationPlan> medicationPlans = medicationPlanRepository.findAll();
        return medicationPlans.stream()
                .map(MedicationPlanBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    //Insert
    public Integer insert(MedicationPlanDTO medicationPlanDTO){
        MedicationPlanFieldValidator.validateInsertOrUpdate(medicationPlanDTO);
/*
        MedicationPlan medicationPlan = medicationPlanRepository
                .findByName(medicationPlanDTO.getName());
        if(medicationPlan != null){
            throw new DuplicateEntryException("Medication plan", "id",
                    medicationPlanDTO.getId());
        }

        return medicationPlanRepository
                .save(MedicationPlanBuilder
                        .generateEntityFromDTO(medicationPlanDTO))
                .getId();
                */

        Integer patientId = medicationPlanDTO.getPatientId();
        Optional<Patient> patient = patientRepository.findById(patientId);

        if(!patient.isPresent()){
            throw new ResourceNotFoundException("patient", "not found with id", medicationPlanDTO.getPatientId());
        }

        MedicationPlan medicationPlan = new MedicationPlan(medicationPlanDTO.getName(), medicationPlanDTO.getTimesADay(),
                medicationPlanDTO.getHourInterval(), medicationPlanDTO.getMedicationList());
        medicationPlan.setPatient(patient.get());
        medicationPlanRepository.save(medicationPlan);
        return medicationPlan.getId();
    }

    //Update
    public Integer update(MedicationPlanDTO medicationPlanDTO){
        MedicationPlan medicationPlan = medicationPlanRepository.findByName(medicationPlanDTO.getName());
        if(medicationPlan == null){
            throw new ResourceNotFoundException("Medication plan","id", medicationPlanDTO.getName());
        }
        MedicationPlanFieldValidator.validateInsertOrUpdate(medicationPlanDTO);
        medicationPlanDTO.setId(medicationPlan.getId());
        return medicationPlanRepository.save(MedicationPlanBuilder.generateEntityFromDTO(medicationPlanDTO)).getId();
    }

    //Delete
    public void delete(MedicationPlanViewDTO medicationPlanViewDTO){
        MedicationPlan medicationPlan = medicationPlanRepository.findByName(medicationPlanViewDTO.getName());
        if(medicationPlan == null){
            throw new ResourceNotFoundException("Medication plan", "id", medicationPlanViewDTO.getId());
        }
        this.medicationPlanRepository.deleteById(medicationPlan.getId());
    }

}
