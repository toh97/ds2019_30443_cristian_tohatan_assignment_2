package com.example.springdemo.services;

import com.example.springdemo.SpringDemoApplicationTests;
import com.example.springdemo.dto.doctorDTOs.DoctorDTO;
import com.example.springdemo.dto.doctorDTOs.DoctorViewDTO;
import com.example.springdemo.errorhandler.IncorrectParameterException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class DoctorServiceTest extends SpringDemoApplicationTests {

    @Autowired
    DoctorService doctorService;


    @Test(expected = IncorrectParameterException.class)
    public void insertDTOBad() {
        DoctorDTO doctorDTO = new DoctorDTO();
        doctorDTO.setEmail("john.patterson.gmail");
        doctorService.insert(doctorDTO);

    }
    @Test
    public void insertDTOGood() {
        DoctorDTO doctorDTO = new DoctorDTO();
        doctorDTO.setName("John Patterson");
        doctorDTO.setEmail("john.patterson@gmail.com");
        doctorDTO.setIban("8443 2212 1102 4042");
        doctorDTO.setAddress("George Baritiu nr. 22");
        Integer id = doctorService.insert(doctorDTO);
        DoctorViewDTO doctor2 = doctorService.findDoctorById(id);
        assert(!doctorDTO.equals(doctor2));

    }
}
